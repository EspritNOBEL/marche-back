package ny.demoback;

import ny.demoback.exposant.Exposant;
import ny.demoback.exposant.ExposantRepository;
import ny.demoback.marche.Marche;
import ny.demoback.marche.MarcheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class InitialDataLoader implements ApplicationRunner {

    private MarcheRepository marcheRepository;
    private ExposantRepository exposantRepository;

    @Autowired
    public InitialDataLoader(
            MarcheRepository marcheRepository,
            ExposantRepository exposantRepository
    ) {
        this.marcheRepository = marcheRepository;
        this.exposantRepository = exposantRepository;
    }

    public void run(ApplicationArguments args) {

        Set<Exposant> Ex_0 = new HashSet<>();

        Marche marche_1 = new Marche(Ex_0, "Marché des Lices", "Place des Lices", "Samedi", "8h", "13h", 120, 110, 50, "Marc FRUTIER", "Rennes");
        Marche marche_2 = new Marche(Ex_0, "Marché de Bréquigny", "Square Sarah Bernhardt", "Vendredi", "8h", "12h", 50, 40, 20, "Jean VALON", "Rennes");
        Marche marche_3 = new Marche(Ex_0, "Marché de Villejean", "Rue de Bourgogne", "Vendredi", "8h", "12h", 35, 30, 20, "Christophe LEGENDRE", "Rennes");
        Marche marche_4 = new Marche(Ex_0, "Marché Sainte Thérèse", "Place du souvenir", "Mercredi", "8h", "13h", 70, 65, 15, "Louis DURUIS", "Rennes");
        Marche marche_5 = new Marche(Ex_0, "Marché Jeanne d'Arc", "Boulevard Alexis Carrel", "Jeudi", "8h", "12h", 55, 40, 25, "Stéphane POULET", "Rennes");

        Set<Marche> M_1 = new HashSet<Marche>();
        M_1.add(marche_1);

        Set<Marche> M_2 = new HashSet<Marche>();
        M_2.add(marche_2);
        M_2.add(marche_3);

        Exposant exposant_1 = new Exposant("SARL du moulin", "Jean MARIN", M_1);
        Exposant exposant_2 = new Exposant("Chez Jeanot", "Jean MORIS", M_1);
        Exposant exposant_3 = new Exposant("Fromagerie Untel", "Marie LEROY", M_1);
        Exposant exposant_4 = new Exposant("Poissonnerie Sapin", "Bernard KEROU", M_2);
        Exposant exposant_5 = new Exposant("SARL Dumont", "Marc GENTIL", M_2);

        exposantRepository.save(exposant_1);
        exposantRepository.save(exposant_2);
        exposantRepository.save(exposant_3);
        exposantRepository.save(exposant_4);
        exposantRepository.save(exposant_5);

        Set<Exposant> Ex_1 = new HashSet<>();
        Ex_1.add(exposant_1);
        Ex_1.add(exposant_2);
        Ex_1.add(exposant_3);
        Ex_1.add(exposant_4);
        Ex_1.add(exposant_5);

        Set<Exposant> Ex_2 = new HashSet<>();
        Ex_1.add(exposant_1);
        Ex_1.add(exposant_2);

        marche_1.setExposantSet(Ex_1);
        marche_2.setExposantSet(Ex_1);
        marche_3.setExposantSet(Ex_1);
        marche_4.setExposantSet(Ex_2);
        marche_5.setExposantSet(Ex_2);

        marcheRepository.save(marche_1);
        marcheRepository.save(marche_2);
        marcheRepository.save(marche_3);
        marcheRepository.save(marche_4);
        marcheRepository.save(marche_5);

    }
}
