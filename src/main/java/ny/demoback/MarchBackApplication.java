package ny.demoback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarchBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarchBackApplication.class, args);
    }

}
