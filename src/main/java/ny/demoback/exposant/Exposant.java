package ny.demoback.exposant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import ny.demoback.marche.Marche;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "exposants")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Exposant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date postedAt = new Date();

    @NonNull
    private String nomCommercialExposant;

    @NonNull
    private String nomResponsableExposant;

    @NonNull
    @JsonIgnore
    @ManyToMany(mappedBy = "exposantSet")
    private Set<Marche> marcheSet;

}
