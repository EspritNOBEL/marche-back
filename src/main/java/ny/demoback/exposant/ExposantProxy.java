package ny.demoback.exposant;

import ny.demoback.marche.Marche;
import ny.demoback.marche.MarcheProxy;
import ny.demoback.marche.MarcheServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("exposant")
public class ExposantProxy {

    Logger logger = LoggerFactory.getLogger(MarcheProxy.class);

    @GetMapping("/")
    public String test() {
        logger.info("Traitement exposantProxy");
        return "Traitement exposantProxy";
    }

    @Autowired
    private ExposantServiceImpl exposantServiceImpl;

    @GetMapping("/findAll")
    public List<Exposant> findAll() {
        logger.info("Traitement exposantProxy/findAll");
        return this.exposantServiceImpl.findAll();
    }

}
