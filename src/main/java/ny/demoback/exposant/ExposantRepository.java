package ny.demoback.exposant;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExposantRepository extends CrudRepository<Exposant, Long> {

    List<Exposant> findAll();

}
