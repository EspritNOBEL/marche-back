package ny.demoback.exposant;

import java.util.List;

public interface ExposantService {

    List<Exposant> findAll();

}
