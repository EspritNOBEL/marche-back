package ny.demoback.exposant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExposantServiceImpl implements ExposantService {

    @Autowired
    private ExposantRepository exposantRepository;

    @Override
    public List<Exposant> findAll() {
        return this.exposantRepository.findAll();
    }
}
