package ny.demoback.marche;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import ny.demoback.exposant.Exposant;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "marches")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Marche {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date postedAt = new Date();

    @NonNull
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "marche_exposant")
    private Set<Exposant> exposantSet;

    @NonNull
    private String nomMarche;

    @NonNull
    private String lieuMarche;

    @NonNull
    private String jourMarche;

    @NonNull
    private String heureDebutMarche;

    @NonNull
    private String heureFinMarche;

    @NonNull
    private int nbStandsMaxMarche;

    @NonNull
    private int nbStandsActuelMarche;

    @NonNull
    private int prixMetreMarche;

    @NonNull
    private String placierMarche;

    @NonNull
    private String communeMarche;

}
