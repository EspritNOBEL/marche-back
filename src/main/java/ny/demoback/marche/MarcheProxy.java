package ny.demoback.marche;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("marche")
public class MarcheProxy {

    Logger logger = LoggerFactory.getLogger(MarcheProxy.class);

    @GetMapping("/")
    public String test() {
        logger.info("Traitement marcheProxy");
        return "Traitement marcheProxy";
    }

    @Autowired
    private MarcheServiceImpl customerServiceImpl;

    @PostMapping("/create")
    public void create(@RequestBody Marche marche) {
        logger.info("Traitement marcheProxy/create");
        this.customerServiceImpl.create(marche);
    }

    @GetMapping("/findAll")
    public List<Marche> findAll() {
        logger.info("Traitement marcheProxy/findAll");
        return this.customerServiceImpl.findAll();
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        logger.info("Traitement marcheProxy/deleteById");
        this.customerServiceImpl.deleteById(id);
    }

    @PutMapping("/update")
    public void update(@RequestBody Marche marche) {
        logger.info("Traitement marcheProxy/update");
        this.customerServiceImpl.update(marche);
    }
    //@RequestBody PartnerSearchWrapper<Partner> searchWrapper

}
