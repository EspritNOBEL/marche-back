package ny.demoback.marche;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface MarcheRepository extends CrudRepository<Marche, Long> {

    List<Marche> findAll();

}
