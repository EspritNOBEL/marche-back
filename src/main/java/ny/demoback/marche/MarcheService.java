package ny.demoback.marche;

import java.util.List;

public interface MarcheService {

    void create(Marche marche);
    List<Marche> findAll();
    void deleteById(Long id);
    void update(Marche marche);

}
