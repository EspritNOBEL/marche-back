package ny.demoback.marche;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarcheServiceImpl implements MarcheService {

    @Autowired
    private MarcheRepository marcheRepository;

    @Override
    public void create(Marche marche) { this.marcheRepository.save(marche); }

    @Override
    public List<Marche> findAll() {
        return this.marcheRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        this.marcheRepository.deleteById(id);
    }

    @Override
    public void update(Marche marche) { this.marcheRepository.save(marche); }

}
